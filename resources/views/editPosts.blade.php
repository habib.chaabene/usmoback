@extends('layouts.app')

@section('content')
<div class="container">
  

    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Modify  posts </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="{{route('home') }}">Back  </a>
        </div>
  </div>
<form method="POST" action="{{ url('update/posts/'.$post->id)}}" enctype="multipart/form-data">
    @csrf
    <div class="col-xs-6 col-sm-6 col-md-6" >

<div class="form-group">
  <strong>Title</strong>
<input type="text" name="title" class="form-control" placeholder="title" value="{{ $post->title}}"/>
</div>

    </div>

    <div class="col-xs-6 col-sm-6 col-md-6" >

      <div class="form-group">
        <strong>Description</strong>
        <textarea type="text" name="description" class="form-control" >{{ $post->description}}</textarea>
      </div>
      
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6" >

            <div class="form-group">
              <strong>Image</strong>
              <input type="file" name="image" class="form-control" placeholder="image"/>
            </div>
            

            <div class="form-group">
                <strong>old Image</strong>
               <div>
              <img src="http://usmonastir.org.tn/usmo/storage/app/public/{{$post->image}}" height="70px;" width="80px;"/>
             {{--      <img src="http://localhost:8000/storage/{{$post->image}}" height="70px;" width="80px;"/> --}}  

                <input type="hidden" name="old_image" value="{{$post->image}}"/>
            </div> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12" >

                 <button  type="submit" class="btn btn-primary"> ajouter</button>
                      </div>

  </form>
  
 
</div> 

@endsection
