@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row" >
    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Posts List </h2>
        </div>

        <div class="pull-right">
            <a class="btn btn-info" href="{{route('posts.index') }}">add your post </a>
        </div>
  </div>
</div> 
 @if($message = Session::get('success'))
 <div class="alert alert-success" role="alert">
   {{ $message}}
  </div>
  @endif
  <table class="table table-bordered">
    <tr>
        <th width="150px">title</th>
        <th width="150px">description</th>
        <th width="150px">categorie</th>
        <th width="150px">date creation</th>
        <th width="150px">image</th>
        <th width="280px">action</th>
    </tr>

    @foreach( $posts  as $post)
    <tr>
       
        <td>  {{ $post->title}}</td>
        <td>{{ $post->description}}</td>
        <td>{{ $post->categorie}}</td>
        <td>{{ $post->created_at}}</td>
 <td><img src="http://usmonastir.org.tn/usmo/storage/app/public/{{$post->image}}" height="70px;" width="80px;"/></td>
     {{--   <td><img src="http://localhost:8000/storage/{{$post->image}}" height="70px;" width="80px;"/></td>   --}}
        <td>
            <a class="btn btn-info" href="">show </a>
        <a class="btn btn-primary" href="{{URL::to('edit/posts/'.$post->id)}}">Edit </a>
            <a class="btn btn-danger" href="{{URL::to('delete/posts/'.$post->id)}}"
                onclick="return confirm('Are you sure')">Delete </a>

        </td>
     
    </tr>
    @endforeach
  </table>
  
 


@endsection
