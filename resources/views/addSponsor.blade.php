@extends('layouts.app')

@section('content')
<div class="container">
  

    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Add list </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="{{route('sponsor') }}">Back  </a>
        </div>
  </div>
<form method="POST" action="{{ route('sponsor.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="col-xs-6 col-sm-6 col-md-6" >
<div class="form-group">
  <strong>Website</strong>
  <input type="text" name="website" class="form-control" placeholder="website"/>
</div>
    </div>
          <div class="col-xs-6 col-sm-6 col-md-6" >

            <div class="form-group">
              <strong>Image</strong>
              <input type="file" name="image" class="form-control" placeholder="image"/>
            </div>
          </div>
       
                <div class="col-xs-12 col-sm-12 col-md-12" >

                 <button  type="submit" class="btn btn-primary"> ajouter</button>
                      </div>

  </form>
  
 
</div> 

@endsection
