@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row" >
    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Effectif List </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="{{route('effectifBasket.create') }}">add your Effectif </a>
        </div>
  </div>
</div> 
 @if($message = Session::get('success'))
 <div class="alert alert-success" role="alert">
   {{ $message}}
  </div>
  @endif
  <table class="table table-bordered">
    <tr>
        <th width="150px">name</th>
        <th width="150px">post</th>
        <th width="150px">Date</th>
        <th width="150px">image</th>
        <th width="280px">action</th>
    </tr>

    @foreach( $EffectifBasket  as $Ef)
    <tr>  
        <td>  {{ $Ef->name}}</td>
        <td>{{ $Ef->post}}</td>
        <td>{{ $Ef->created_at}}</td>
 <td><img src="http://usmonastir.org.tn/usmo/storage/app/public/{{$Ef->image}}" height="70px;" width="80px;"/></td>
   {{--     <td><img src="http://localhost:8000/storage/{{$Ef->image}}" height="70px;" width="80px;"/></td>   --}}
        <td>
            <a class="btn btn-info" href="">show </a>
        <a class="btn btn-primary" href="{{URL::to('edit/effectifBasket/'.$Ef->id)}}">Edit </a>
            <a class="btn btn-danger" href="{{URL::to('delete/effectifBasket/'.$Ef->id)}}"
             onclick="return confirm('Are you sure')">Delete </a>
        </td>
     
    </tr>
    @endforeach
  </table>
  
 


@endsection
