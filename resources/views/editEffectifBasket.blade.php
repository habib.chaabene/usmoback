@extends('layouts.app')

@section('content')
<div class="container">
  
    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Modify  Effectif Basket  </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="{{route('effectifBasket') }}">Back  </a>
        </div>
  </div>
<form method="POST" action="{{ url('update/effectifBasket/'.$effectifBasket->id)}}" enctype="multipart/form-data">
    @csrf
    <div class="col-xs-6 col-sm-6 col-md-6" >

<div class="form-group">
  <strong>Name</strong>
<input type="text" name="name" class="form-control" placeholder="title" value="{{ $effectifBasket->name}}"/>
</div>

    </div>


<div class="col-xs-6 col-sm-6 col-md-6" >

        <div class="form-group">
          <label for="exampleFormControlSelect1">POST  select</label>
          <select class="form-control"  id="post" name="post">
            @foreach ($items as $i)
          <option value="{{$i}}" @if($effectifBasket->post=== $i) selected='selected' @endif>{{$i}}</option>
          @endforeach
          </select>
          <input type="hidden" name="old_Post" value="{{$effectifBasket->post}}"/>  
        </div>
      </div>
          <div class="col-xs-6 col-sm-6 col-md-6" >

            <div class="form-group">
              <strong>Image</strong>
              <input type="file" name="image" class="form-control" placeholder="image"/>
            </div>
            

            <div class="form-group">
                <strong>old Image</strong>
               <div>
               <img src="http://usmonastir.org.tn/usmo/storage/app/public/{{$effectifBasket->image}}" height="70px;" width="80px;"/>
          {{--        <img src="http://localhost:8000/storage/{{$effectifBasket->image}}" height="70px;" width="80px;"/> --}}

                <input type="hidden" name="old_image" value="{{$effectifBasket->image}}"/>
            </div> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12" >

                 <button  type="submit" class="btn btn-primary"> Edit </button>
                      </div>

  </form>
  
 
</div> 

@endsection
