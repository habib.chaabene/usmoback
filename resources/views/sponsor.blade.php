@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row" >
    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>sponsor  List </h2>
        </div>

        <div class="pull-right">
            <a class="btn btn-info" href="{{route('sponsor.create') }}">add your Sponsor </a>
        </div>
  </div>
</div> 
 @if($message = Session::get('success'))
 <div class="alert alert-success" role="alert">
   {{ $message}}
  </div>
  @endif
  <table class="table table-bordered">
    <tr>
        <th width="150px">website</th>
        <th width="150px">date creation</th>
        <th width="150px">image</th>
        <th width="280px">action</th>
    </tr>

    @foreach( $sponsor  as $s)
    <tr>
       
        <td>  {{ $s->website}}</td>
        <td>{{ $s->created_at}}</td>
  <td><img src="http://usmonastir.org.tn/usmo/storage/app/public/{{$s->image}}" height="70px;" width="80px;"/></td>
     {{--  <td><img src="http://localhost:8000/storage/{{$s->image}}" height="70px;" width="80px;"/></td>   --}}
        <td>
            <a class="btn btn-info" href="">show </a>
        <a class="btn btn-primary" href="{{URL::to('edit/sponsor/'.$s->id)}}">Edit </a>
            <a class="btn btn-danger" href="{{URL::to('delete/sponsor/'.$s->id)}}"
                onclick="return confirm('Are you sure')">Delete </a>

        </td>
     
    </tr>
    @endforeach
  </table>

@endsection
