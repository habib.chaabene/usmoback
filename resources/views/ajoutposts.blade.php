@extends('layouts.app')

@section('content')
<div class="container">
  

    <div class="col-lg-12 margin-tb">
    
        <div class="pull-left">
            <h2>Add list </h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-info" href="{{route('home') }}">Back  </a>
        </div>
  </div>
<form method="POST" action="{{ route('posts.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="col-xs-6 col-sm-6 col-md-6" >

<div class="form-group">
  <strong>Title</strong>
  <input type="text" name="title" class="form-control" placeholder="title"/>
</div>

    </div>

    <div class="col-xs-6 col-sm-6 col-md-6" >

      <div class="form-group">
        <strong>Description</strong>
        <textarea type="text" name="description" class="form-control" >Description</textarea>
      </div>
      
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6" >

            <div class="form-group">
              <strong>Image</strong>
              <input type="file" name="image" class="form-control" placeholder="image"/>
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6" >

            <div class="form-group">
              <label for="exampleFormControlSelect1">Example select</label>
              <select class="form-control"  id="type" name="type">
                @foreach ($items as $i)
              <option value="{{$i}}">{{$i}}</option>
              @endforeach
              </select>
            </div>
          </div>
               
                <div class="col-xs-12 col-sm-12 col-md-12" >

                 <button  type="submit" class="btn btn-primary"> ajouter</button>
                      </div>

  </form>
  
 
</div> 

@endsection
