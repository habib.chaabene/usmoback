<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome');});


// routes POSTS
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ajouterPosts','HomeController@create')->name('posts.index')->middleware('auth');
Route::post('/store','HomeController@store')->name('posts.store')->middleware('auth');
Route::post('/update/posts/{id}','HomeController@update')->name('posts.update')->middleware('auth');
Route::get('/delete/posts/{id}','HomeController@Delete')->name('posts.delete')->middleware('auth');
Route::get('/edit/posts/{id}','HomeController@edit')->name('posts.edit')->middleware('auth');
Route::get('/post/{id}','postsController@show')->middleware('auth');

// routes SPONSOR
Route::get('/sponsor', 'SponsorController@index')->name('sponsor')->middleware('auth');
Route::get('/addSponsor','SponsorController@create')->name('sponsor.create')->middleware('auth');
Route::post('/storeSponsor','SponsorController@store')->name('sponsor.store')->middleware('auth');
Route::get('/edit/sponsor/{id}','SponsorController@edit')->name('sponsor.edit')->middleware('auth');
Route::post('/update/sponsor/{id}','SponsorController@update')->name('sponsor.update')->middleware('auth');
Route::get('/delete/sponsor/{id}','SponsorController@Delete')->name('sponsor.delete')->middleware('auth');

// routes EFFECTIF 
Route::get('/effectifBasket', 'EffectifFootController@index')->name('effectifBasket')->middleware('auth');
Route::get('/addeffectifBasket','EffectifFootController@create')->name('effectifBasket.create')->middleware('auth');
Route::post('/storeeffectifBasket','EffectifFootController@store')->name('effectifBasket.store')->middleware('auth');
Route::get('/edit/effectifBasket/{id}','EffectifFootController@edit')->name('effectifBasket.edit')->middleware('auth');
Route::post('/update/effectifBasket/{id}','EffectifFootController@update')->name('effectifBasket.update')->middleware('auth');
Route::get('/delete/effectifBasket/{id}','EffectifFootController@Delete')->name('effectifBasket.delete')->middleware('auth');


Auth::routes(/* [ 'register' => false ] */);
