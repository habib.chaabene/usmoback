<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EffectifBasket extends Model
{
    //
    protected $table = 'effectif_basket';
    protected $fillable =  [
        'name',
        'post' , 
        'image',
        'created_at',
        'updated_at'
    ];
}
