<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    //
    protected $table = 'posts';
    protected $fillable =  [
        'title','description' , 
        'image', 'categorie',
        'created_at','updated_at'
    ];
}
