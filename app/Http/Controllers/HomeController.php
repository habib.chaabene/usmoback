<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\posts ; 
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = posts::all();
        return view('home',['posts'=> $posts]);
      
    }
    public function create()
    {
        $items = ['foot', 'basket'];
        return view('ajoutposts',['items'=> $items]);
      
    }
    public function store(Request $request)
    {
            
        $data = array();
        $data['title']= $request->title ; 
        $data['description'] = $request->description ; 
        $image = $request->file('image');
        $data['categorie'] = $request->type ; 
        $data['created_at'] = Carbon::now()->toDateTimeString();
        $data['updated_at'] = Carbon::now()->toDateTimeString();
        if($image)
        {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
           // $sucess = $image->move($upload_path,$image_full_name);
            $pathToFile = Storage::disk('public')->put('uploads', $image);
            $data['image']= $pathToFile ; 
            $post = DB::table('posts')->insert($data); 
           
            return redirect()->route('home')->with('success','posts created Successfuly');
        }
        
           /*   $exists = Storage::disk('ftp')->exists('file.jpg');
            $filenamewithextension = $request->file('image')->getClientOriginalExtension();
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename = pathinfo($filenamewithextension , PATHINFO_FILENAME);
            $filenamestore = $filename.'_'.uniqid().'.'.$extension;
            $store =    Storage::disk('ftp')->put($filenamestore , fopen($request->file('image'),'r+')); 
            die( $store); */
        

        /* $data = array();
        $data['title']= $request->title ; 
        $data['description'] = $request->description ; 


        $image = $request->file('image');
        if($image)
        {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
           // $sucess = $image->move($upload_path,$image_full_name);
            $pathToFile = Storage::disk('public')->put('uploads/', $image);
            $data['image']= $pathToFile ; 
            $post = DB::table('posts')->insert($data);
            return redirect()->route('home')->with('success','posts created Successfuly');
        } */
    }

    public function edit($id)
    {
        $post = DB::table('posts')->where('id',$id)->first();
        return view('editPosts',['post'=>$post]);
    }
    public function update(Request $request , $id)
    {
        $oldImage = $request->old_image ; 
        
        $data = array();
        $data['title']= $request->title ; 
        $data['description'] = $request->description ; 


        $image = $request->file('image');
        if($image)
        {
            
           
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
         //   $sucess = $image->move($upload_path,$image_full_name);
           $pathToFile = Storage::disk('public')->put('uploads/', $image);


            $data['image']= $pathToFile ; 
            $post = DB::table('posts')->where('id',$id)->update($data);
            return redirect()->route('home')->with('success','posts updated Successfuly');
        }
        else
        {     
        $data = array();
        $data['title']= $request->title ; 
        $data['description'] = $request->description ; 
        $data['image']= $oldImage ; 
        $post = DB::table('posts')->where('id',$id)->update($data);
        return redirect()->route('home')->with('success','posts updated Successfuly');
        }
    }

    public function Delete($id)
    {
        $data = DB::table('posts')->where('id',$id)->first();
         $image = $data->image ; 
        $post = DB::table('posts')->where('id',$id)->delete();
        return redirect()->route('home')->with('success','posts Deleted Successfuly');

    }
}
