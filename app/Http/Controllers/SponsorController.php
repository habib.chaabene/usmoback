<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sponsor ; 
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;
class SponsorController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sponsor = Sponsor::all();
        return view('sponsor',['sponsor'=> $sponsor]);
    }
   public function create()
    {
        return view('addSponsor');
    }
   public function store(Request $request)
    {   
        $data = array();
        $data['website']= $request->website ; 
        $image = $request->file('image');
        $data['created_at'] = Carbon::now()->toDateTimeString();
        $data['updated_at'] = Carbon::now()->toDateTimeString();
        if($image)
        {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
           // $sucess = $image->move($upload_path,$image_full_name);
            $pathToFile = Storage::disk('public')->put('uploads', $image);
            $data['image']= $pathToFile ; 
            $sponsor = DB::table('sponsor')->insert($data); 
           
            return redirect()->route('sponsor')->with('success','sponsor created Successfuly');
        }
    }
  
    public function edit($id)
    {
        $sponsor = DB::table('sponsor')->where('id',$id)->first();
        return view('editSponsor',['sponsor'=>$sponsor]);
    }
  
    public function update(Request $request , $id)
    {
        $oldImage = $request->old_image ;     
        $data = array();
        $data['website']= $request->website ; 

        $image = $request->file('image');
        if($image)
        {   
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
         //   $sucess = $image->move($upload_path,$image_full_name);
           $pathToFile = Storage::disk('public')->put('uploads/', $image);


            $data['image']= $pathToFile ; 
            $post = DB::table('sponsor')->where('id',$id)->update($data);
            return redirect()->route('sponsor')->with('success','sponsor updated Successfuly');
        }
        else
        {     
        $data = array();
        $data['website']= $request->website ; 
        $data['image']= $oldImage ; 
        $post = DB::table('sponsor')->where('id',$id)->update($data);
        return redirect()->route('sponsor')->with('success','sponsor updated Successfuly');
        }
    }

    public function Delete($id)
    {
        $data = DB::table('sponsor')->where('id',$id)->first();
         $image = $data->image ; 
        $Spons = DB::table('sponsor')->where('id',$id)->delete();
        return redirect()->route('sponsor')->with('success','sponsor Deleted Successfuly');

    } 
}
