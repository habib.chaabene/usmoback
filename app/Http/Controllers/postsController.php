<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posts ; 
use App\Sponsor ;
use App\EffectifBasket ; 
class postsController extends Controller
{
    public function index()
    {
        $posts = posts::all();
        return  response()->json($posts , 200);
    }

    public function indexSponsor()
    {
        $Sponsor = Sponsor::all();
        return  response()->json($Sponsor , 200);
    }
  
    public function  indexEffectifBasket()
    {
        $effectifBasket = EffectifBasket::all();
        return  response()->json($effectifBasket , 200); 
    }

}
