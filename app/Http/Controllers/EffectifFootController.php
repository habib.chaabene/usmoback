<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\EffectifBasket ; 
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;
class EffectifFootController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $EffectifBasket = EffectifBasket::all();
        return view('effectifBasket',['EffectifBasket'=> $EffectifBasket]);
    }
   public function create()
    {
        $items = ['Gardien de but', 'Défenseur','Milieu de terrain','Attaquant'];
        return view('AddEffectifBasket',['items'=> $items]);
    }
    public function store(Request $request)
    {   
        $data = array();
        $data['name']= $request->name ; 
        $data['post']= $request->post ; 
        $image = $request->file('image');
        $data['created_at'] = Carbon::now()->toDateTimeString();
        $data['updated_at'] = Carbon::now()->toDateTimeString();
        if($image)
        {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
           // $sucess = $image->move($upload_path,$image_full_name);
            $pathToFile = Storage::disk('public')->put('uploads', $image);
            $data['image']= $pathToFile ; 
            $EffectifBasket = DB::table('effectif_basket')->insert($data);         
            return redirect()->route('effectifBasket')->with('success','effectifBasket created Successfuly');
        }
    }

    public function edit($id)
    {
        $items = ['Gardien de but', 'Défenseur','Milieu de terrain','Attaquant'];
        $effectifBasket = DB::table('effectif_basket')->where('id',$id)->first();
        return view('editEffectifBasket',['effectifBasket'=>$effectifBasket , 'items'=> $items]);
    }
    public function update(Request $request , $id)
    {
        $oldImage = $request->old_image ;   
        $oldPost = $request->old_Post  ;
        $data = array();
        $data['name']= $request->name ; 
        $image = $request->file('image');
        if($image)
        {   
            $data['post']= $request->post;
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name ; 
         //   $sucess = $image->move($upload_path,$image_full_name);
           $pathToFile = Storage::disk('public')->put('uploads/', $image);


            $data['image']= $pathToFile ; 
            $post = DB::table('effectif_basket')->where('id',$id)->update($data);
            return redirect()->route('effectifBasket')->with('success','effectifBasket updated Successfuly');
        }
        else
        {     
        $data = array();
        $data['name']= $request->name ; 
        $data['post']=  $request->post;
        $data['image']= $oldImage ; 
        $post = DB::table('effectif_basket')->where('id',$id)->update($data);
        return redirect()->route('effectifBasket')->with('success','effectifBasket updated Successfuly');
        }
    }

    public function Delete($id)
    {
        $data = DB::table('effectif_basket')->where('id',$id)->first();
        $image = $data->image ; 
        $Spons = DB::table('effectif_basket')->where('id',$id)->delete();
        return redirect()->route('effectifBasket')->with('success','effectifBasket Deleted Successfuly');

    }  
}

